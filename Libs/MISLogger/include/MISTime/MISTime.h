#pragma once

#include <string>

namespace Logger
{
    namespace MISTime
    {
        std::string GetTime();
        std::string GetDate();
    }
}