// Name: Logger
// Author: Sinclair Ross - sinclair@mckenzieintelligence.co.uk
// Notes:: A series of functions to make logging debug information as easy as possible.
//         There are 4 different types of logging.
//              Error - Outputs a red error to console and log file.
//              Warning - Outputs a yellow warning to console and log file.
//              Message - Outputs a white message to console and log file.
//              DevMessage - Outputs a cyan message to console only.

#pragma once

#include <sstream>
#include <cstring>
#include <string.h>
#include <stdio.h>

#include "MISTime/MISTime.h"

namespace Logger
{
    namespace Internal
    {
        constexpr size_t STR_BUFFER_SIZE = 256;

        enum class ConsoleColour
        {
            WHITE,
            RED,
            YELLOW,
            CYAN
        };

        void OutputToConsole(const char* strBuffer, const ConsoleColour colour);
        void OutputToFile(const char* strBuffer, const char* outputFile);
        void DatedLog(const char* strBuffer);

        template<typename ... Args>
        const std::string CreateMessage(const char* msgType, const char* fmt, const Args ... args)
        {
            char buffer[Internal::STR_BUFFER_SIZE] = "";
            _snprintf_s(buffer + strlen(buffer), STR_BUFFER_SIZE, Internal::STR_BUFFER_SIZE - strlen(buffer), fmt, args...);
            strcat_s(buffer, "\n");

            std::stringstream ss;
            ss << msgType << MISTime::GetTime() << " - " << buffer;

            return ss.str();
        }
    }

    template<typename ... Args>
    void Error(const char* fmt, const Args ... args)
    {
        const std::string msg = Internal::CreateMessage("Error   ", fmt, std::forward<const Args>(args)...);
	    
        Internal::OutputToConsole(msg.c_str(), Internal::ConsoleColour::RED);
        Internal::DatedLog(msg.c_str());
	    
        #ifdef BREAK_ON_ERROR
        __debugbreak();
        #endif
    }

    template<typename ... Args>
    void Warning(const char* fmt, const Args ... args)
    {
       const std::string msg = Internal::CreateMessage("Warning ", fmt, std::forward<const Args>(args)...);
       
       Internal::OutputToConsole(msg.c_str(), Internal::ConsoleColour::YELLOW);
       Internal::DatedLog(msg.c_str());
    
       #ifdef BREAK_ON_WARNING
       __debugbreak();
       #endif
    }

    template<typename ... Args>
    void Message(const char* fmt, const Args ... args)
    {
       const std::string msg = Internal::CreateMessage("Message ", fmt, std::forward<const Args>(args)...);
       
       Internal::OutputToConsole(msg.c_str(), Internal::ConsoleColour::WHITE);
       Internal::DatedLog(msg.c_str());
    }

    template<typename ... Args>
    void DevMessage(const char* fmt, const Args ... args)
    {
      const std::string msg = Internal::CreateMessage("Dev     ", fmt, std::forward<const Args>(args)...);
      Internal::OutputToConsole(msg.c_str(), Internal::ConsoleColour::CYAN);
    }
}