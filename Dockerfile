FROM gcc:10.2.0

RUN su root

RUN apt update
RUN apt -y install python3.7-dev
RUN apt -y install python3-pip

WORKDIR /BoostPython/Boost
COPY boost_1_75_0.tar.gz ./boost_1_75_0.tar.gz
RUN tar xzvf boost_1_75_0.tar.gz
WORKDIR ./boost_1_75_0
RUN ./bootstrap.sh --with-python-version=3.7
RUN ./b2 install

WORKDIR ./stage/lib
RUN ldconfig -v

WORKDIR /ShapeValidator
RUN mkdir ./Bin

COPY ./Source ./Source/
COPY ./Flask ./Flask/

RUN gcc -c -Wall -fPIC Source/main.cpp -o Bin/main.o -std=c++17 -lstdc++ -lstdc++fs \
	-ISource -I/usr/include/python3.7 -I/BoostPython/Boost/boost_1_75_0	\
	-D USE_FILESYSTEM_STANDARD -D PYTHON_BUILD 

RUN gcc -c -Wall -fPIC Source/Logger/Logger.cpp -o Bin/Logger.o																-std=c++17 -lstdc++ -lstdc++fs 					-ISource	-D USE_FILESYSTEM_STANDARD -D BOOST_PYTHON_STATIC_LIB
RUN gcc -c -Wall -fPIC Source/Utils/ShapeComponentTracker/ShapeComponentTracker.cpp -o Bin/ShapeComponentTracker.o			-std=c++17 -lstdc++ -lstdc++fs 					-ISource
RUN gcc -c -Wall -fPIC Source/Utils/MathsHelper.cpp -o Bin/MathsHelper.o                                                   	-std=c++17 -lstdc++ -lstdc++fs  				-ISource
RUN gcc -c -Wall -fPIC Source/Utils/Utils.cpp -o Bin/Utils.o                                                               	-std=c++17 -lstdc++ -lstdc++fs  				-ISource
RUN gcc -c -Wall -fPIC Source/Utils/Vector2.cpp -o Bin/Vector2.o                                                           	-std=c++17 -lstdc++ -lstdc++fs  				-ISource
RUN gcc -c -Wall -fPIC Source/Validator/Validator_Points/Validator_Points.cpp -o Bin/Validator_Points.o                    	-std=c++17 -lstdc++ -lstdc++fs  				-ISource
RUN gcc -c -Wall -fPIC Source/Validator/Validator_Polygon/Validator_Polygon.cpp -o Bin/Validator_Polygon.o                 	-std=c++17 -lstdc++ -lstdc++fs  				-ISource
RUN gcc -c -Wall -fPIC Source/Validator/Validator_PolyLine/Validator_PolyLine.cpp -o Bin/Validator_PolyLine.o              	-std=c++17 -lstdc++ -lstdc++fs  				-ISource
RUN gcc -c -Wall -fPIC Source/VertexReader/VertexReader_Points/VertexReader_Points.cpp -o Bin/VertexReader_Points.o        	-std=c++17 -lstdc++ -lstdc++fs  				-ISource
RUN gcc -c -Wall -fPIC Source/VertexReader/VertexReader_Polygon/VertexReader_Polygon.cpp -o Bin/VertexReader_Polygon.o     	-std=c++17 -lstdc++ -lstdc++fs  				-ISource
RUN gcc -c -Wall -fPIC Source/VertexReader/VertexReader_PolyLine/VertexReader_PolyLine.cpp -o Bin/VertexReader_PolyLine.o  	-std=c++17 -lstdc++ -lstdc++fs					-ISource
			
WORKDIR ./Bin
RUN gcc -Wall -shared \
	main.o \
	Logger.o \
	ShapeComponentTracker.o \
	MathsHelper.o \
	Utils.o \
	Vector2.o \
	Validator_Points.o \
	Validator_Polygon.o \
	Validator_PolyLine.o \
	VertexReader_Points.o \
	VertexReader_Polygon.o \
	VertexReader_PolyLine.o \
	-o ShapeValidator.so \ 
	-D BOOST_PYTHON_STATIC_LIB \
	-std=c++17 -lstdc++ -lstdc++fs \
	-lpython3.7m \
	-lboost_python37
	
RUN cp ./ShapeValidator.so /ShapeValidator/Flask/
WORKDIR /ShapeValidator/Flask/
RUN pip3 install -r requirements.txt
#RUN python3 ./app.py
