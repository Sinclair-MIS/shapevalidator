import os
import shutil

from flask import Flask, render_template, request, send_file
from flask_uploads import UploadSet, configure_uploads, ARCHIVES
import zipfile
from ShapeValidator import Test, ValidateFile

uploadSet = UploadSet('Files', ARCHIVES)

app = Flask(__name__)
app.config['UPLOADED_FILES_DEST'] = './TempStorage/'
configure_uploads(app, uploadSet)


@app.route('/')
def home():
    return render_template("home.html")


@app.route('/Working')
def Working():
    return render_template("working.html")


@app.route('/Validate', methods=['POST'])
def Validate():
    out: str = "Invalid"

    if 'File' in request.files:
        filename_full = uploadSet.save(request.files['File'])
        filename = filename_full.split('.')[0]

        with zipfile.ZipFile(f'TempStorage/{filename_full}', 'r') as zip:
            zip.extractall(f'TempStorage/{filename}')

        os.remove(f'TempStorage/{filename_full}')

        submission_type = request.form['btn_submit']
        if submission_type == 'SUBMIT':
            if ValidateFile(filename):
                out = "Valid"

        shutil.rmtree(f'TempStorage/{filename}')

    return out


print("Beware I live!")
print(Test())

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
