const delta_time_millis = 16;
const delta_time_seconds = delta_time_millis * 0.001;

class Point
{
    constructor(x, y)
    {
        this.X = x;
        this.Y = y;
    }

    Set(x,y) { this.X = x; this.Y = y;}
    CalculateLength_Sqr()  { return this.X*this.X + this.Y*this.Y; }
    CalculateLength() { return Math.sqrt(this.CalculateLength_Sqr()); }

    Normalise()
    {
        let length = this.CalculateLength();
        if(length < 0.001) { length = 1.0; }
        this.X /= length;
        this.Y /= length;
    }

    Normalised()
    {
        let out = new Point(this.X, this.Y);
        out.Normalise();
        return out;
    }
}

function Clamp(val, min, max)
{
    return (max * (val > max)) +
        (min * (val < min)) +
        (val * (val >= min && val <= max));
}

function Normalise(val, min, max)
{
    return Clamp((val - min) / (max - min), 0.0, 1.0);
}

let Cursor = new Point(0,0);
let Content = document.getElementById("ID_Content");
let Marker = document.getElementById("ID_Marker");
let Anchor = GetAnchorPos()

function GetAnchorPos()
{
    let rect_Content = Marker.getBoundingClientRect();
    return new Point(rect_Content.width/2, rect_Content.height/2);
}

function update()
{
    Anchor = GetAnchorPos();
    let toAnchor = new Point(Anchor.X - Cursor.X, Anchor.Y - Cursor.Y);
    let pos = new Point(toAnchor.X/70, toAnchor.Y/70);

    console.log(`X:${pos.X} - Y:${pos.Y}`)

    Content.style.left = `${pos.X}px`;
    Content.style.top = `${pos.Y}px`;
}

function main()
{
    document.addEventListener('mousemove', function(e)
    {
        Cursor.Set(e.offsetX, e.offsetY);
        update();
    });
}

main();