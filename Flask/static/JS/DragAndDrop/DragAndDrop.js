let DropArea = document.getElementById("ID_DropBox");
let DropText = document.getElementById("ID_DropBoxText");
let DropImage = document.getElementById("ID_DropBoxImage");
let Input = document.getElementById("ID_FileDrop")



function OnSelection(e)
{
    DropText.innerHTML = Input.files[0].name;
    DropText.setAttribute("class", "drop-input-label-active");
}

function OnDrop(e)
{
    BlockDefaults(e);
    DropText.innerHTML = e.dataTransfer.files.item(0).name;
    DropText.setAttribute("class", "drop-input-label-active");

    Input.files = e.dataTransfer.files;
}

function OnDragEnter(e)
{
    BlockDefaults(e);
    DropImage.setAttribute("class", "drop-input-image-hover");
}

function OnDragOver(e)
{
    BlockDefaults(e);
}

function OnDragExit(e)
{
    BlockDefaults(e);
    DropImage.setAttribute("class", "drop-input-image");
}

function BlockDefaults(e)
{
  e.preventDefault();
  e.stopPropagation();
}

Input.addEventListener('change', OnSelection, false)
DropArea.addEventListener('dragenter', OnDragEnter, false)
DropArea.addEventListener('dragover', OnDragOver, false)
DropArea.addEventListener('drop', OnDrop, false)
DropArea.addEventListener('dragleave', OnDragExit, false)