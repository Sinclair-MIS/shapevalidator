let Input_Username = document.getElementById("ID_TB_UserName")
let Input_Password = document.getElementById("ID_TB_Password")

function Validate()
{
    let validated = true;

    let username = document.forms["Form_ManualSubmission"]["UserName"].value;
    if(username === "")
    {
        Input_Username.setAttribute("class", "input_validate");
        validated = false;
    }

    return validated;
}