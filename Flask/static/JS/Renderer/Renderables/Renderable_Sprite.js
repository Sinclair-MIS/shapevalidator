// import { CreateShaderProgram } from "./ShaderHelpers.js";

//class Renderable_Sprite
//{
//  constructor(glContext)
//  {
//    const shaderProgram = CreateShaderProgram(this.Shader_Fragment, this.Shader_Vertex);
//
//    this.ProgramInfo = {
//      program: shaderProgram,
//      attribLocations: {
//        vertexPosition: glContext.getAttribLocation(shaderProgram, 'aVertexPosition'),
//      },
//
//      uniformLocations: {
//        projectionMatrix: glContext.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
//        modelViewMatrix: glContext.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
//      },
//  };
//
//  this.PositionBuffer = glContext.createBuffer();
//  glContext.bindBuffer(glContext.ARRAY_BUFFER, this.PositionBuffer);
//
//  const positions = [
//    -1.0,  1.0,
//     1.0,  1.0,
//    -1.0, -1.0,
//     1.0, -1.0,
//    ];
//
//    glContext.bufferData(glContext.ARRAY_BUFFER, new Float32Array(positions), glContext.STATIC_DRAW);
//  }
//
//  Draw(glContext, vpMatrix, modelMatrix)
//  {
//    glContext.bindBuffer(glContext.ARRAY_BUFFER, this.PositionBuffer.position);
//    glContext.vertexAttribPointer(
//        this.ProgramInfo.attribLocations.vertexPosition,
//        2,
//        glContext.FLOAT,
//        false,
//        0,
//        0);
//    glContext.enableVertexAttribArray(this.ProgramInfo.attribLocations.vertexPosition);
//    glContext.useProgram(this.ProgramInfo.program);
//
//    glContext.uniformMatrix4fv(this.ProgramInfo.uniformLocations.projectionMatrix, vpMatrix);
//    glContext.uniformMatrix4fv(this.ProgramInfo.uniformLocations.modelViewMatrix, false, modelViewMatrix);
//    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
//  }
//
//  static Shader_Vertex = `
//    attribute vec4 aVertexPosition;
//
//    uniform mat4 u_VPMatrix;
//    uniform mat4 u_Transform;
//
//    void main()
//    {
//        gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
//    }`;
//
//  static Shader_Fragment = `
//    void main()
//    {
//        gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
//    }`;
//}
//
//export { Renderable_Sprite }