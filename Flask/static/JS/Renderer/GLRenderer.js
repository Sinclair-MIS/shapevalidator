const delta_time_millis = 16;
const delta_time_seconds = delta_time_millis * 0.001;

function main()
{
  const canvas = document.querySelector('#glCanvas');
  const gl = canvas.getContext('webgl2');

  if (gl)
  {
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    const vpMatrix = mat4.create();
    mat4.perspective(vpMatrix, Math.PI * 0.25, gl.canvas.clientWidth / gl.canvas.clientHeight, 0.1, 100.0);

    let sprite = new Renderable_Sprite(gl);
    let simulation = new Simulation();
    function update()
    {
      simulation.Update();
      drawScene(gl, vpMatrix, sprite, simulation.GetEntities());
    }

    setInterval(update, delta_time_millis);
  }
  else
  {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
  }
}

function drawScene(gl, vpMatrix, sprite, transforms)
{
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  for(i = 0; i < transforms.length; ++i)
  {
    sprite.Draw(gl, vpMatrix, transforms[i]);
  }
}

class Renderable_Sprite
{
  Shader_Vertex = `#version 300 es
    uniform mat4 u_VPMatrix;
    uniform mat4 u_Transform;
    
    in vec2 a_Vertex;
    out vec2 v_FragCoord;
  
    void main() 
    {
        v_FragCoord = a_Vertex;  
        gl_Position = (u_VPMatrix * u_Transform) * vec4(a_Vertex.xy, 0, 1);
    }`;

  Shader_Fragment = `#version 300 es
    precision lowp float;
    
    in vec2 v_FragCoord;
    out vec4 FragColour;
    
    void main()
    {
      float dist = length(v_FragCoord);
      if(dist < 0.9)
      {
        FragColour = vec4(1,1,1,0.4);
      }
      else
      {
        discard;
      }
    }`;

  vertices = [
    -1.0,  1.0,
     1.0,  1.0,
    -1.0, -1.0,
     1.0, -1.0,
    ];


  constructor(gl)
  {
    this.VertexArray = gl.createVertexArray();
    gl.bindVertexArray(this.VertexArray);

    const shaderProgram = CreateShaderProgram(gl, this.Shader_Vertex, this.Shader_Fragment);
    gl.useProgram(shaderProgram);

    this.Handle_VPMatrix = gl.getUniformLocation(shaderProgram, "u_VPMatrix");
    this.Handle_Transform = gl.getUniformLocation(shaderProgram, "u_Transform");

    let vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.vertices), gl.STATIC_DRAW);

    let attrib_Vertex = gl.getAttribLocation(shaderProgram, 'a_Vertex');
    gl.vertexAttribPointer(attrib_Vertex, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(attrib_Vertex);
  }

  Draw(gl, vpMatrix, transform)
  {
    gl.bindVertexArray(this.VertexArray);

    gl.uniformMatrix4fv(this.Handle_VPMatrix, false, vpMatrix);
    gl.uniformMatrix4fv(this.Handle_Transform, false, transform);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
  }
}

class Renderable_ScreenQuad
{
  Shader_Vertex = `#version 300 es
    uniform mat4 u_VPMatrix;
    uniform mat4 u_Transform;
    
    in vec2 a_Vertex;
    in vec2 a_TexCoord;
    
    out vec2 v_TexCoord;
  
    void main() 
    {
        v_TexCoord = a_TexCoord;
        gl_Position = vec4(a_Vertex.xy, 0, 1);
    }`;

  Shader_Fragment = `#version 300 es
    precision lowp float;
    
    in vec2 v_TexCoord;
    out vec4 FragColour;
    
    void main()
    {
        FragColour = vec4(1,1,1,0.4);
    }`;

  vertices = [
    -1.0,  1.0, 0.0, 1.0,
     1.0,  1.0, 1.0, 1.0,
    -1.0, -1.0, 0.0, 0.0,
     1.0, -1.0, 1.0, 0.0
    ];


  constructor(gl)
  {
    this.VertexArray = gl.createVertexArray();
    gl.bindVertexArray(this.VertexArray);

    const shaderProgram = CreateShaderProgram(gl, this.Shader_Vertex, this.Shader_Fragment);
    gl.useProgram(shaderProgram);

    this.Handle_VPMatrix = gl.getUniformLocation(shaderProgram, "u_VPMatrix");
    this.Handle_Transform = gl.getUniformLocation(shaderProgram, "u_Transform");

    let vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.vertices), gl.STATIC_DRAW);

    let attrib_Vertex = gl.getAttribLocation(shaderProgram, 'a_Vertex');
    gl.vertexAttribPointer(attrib_Vertex, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(attrib_Vertex);

   let attrib_TexCoord = gl.getAttribLocation(shaderProgram, 'a_TexCoord');
    gl.vertexAttribPointer(attrib_TexCoord, 2, gl.FLOAT, false, 2, 0);
    gl.enableVertexAttribArray(attrib_TexCoord);
  }

  Draw(gl, vpMatrix, transform)
  {
    gl.bindVertexArray(this.VertexArray);

    gl.uniformMatrix4fv(this.Handle_VPMatrix, false, vpMatrix);
    gl.uniformMatrix4fv(this.Handle_Transform, false, transform);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
  }
}

function CreateShaderProgram(gl, vsSource, fsSource)
{
  const vertexShader = LoadShader(gl, gl.VERTEX_SHADER, vsSource);
  const fragmentShader = LoadShader(gl, gl.FRAGMENT_SHADER, fsSource);

  let shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);

  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS))
  {
    alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
    shaderProgram = null;
  }

  return shaderProgram;
}

function LoadShader(gl, type, source)
{
  let shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
  {
    alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
    gl.deleteShader(shader);
    shader = null;
  }

  return shader;
}

class Simulation
{
  constructor()
  {
    this.Entities = [];
    this.CreateEntity(5, 5, -60);
    this.CreateEntity(0, 2, -20);
    this.CreateEntity(0, 0, -40);
  }

  Update()
  {
    for(let i = 0; i < this.Entities.length; ++i)
    {
      mat4.translate(this.Entities[i], this.Entities[i], [ 10.0 * delta_time_seconds, 0.0, 0.0]);
    }
  }

  CreateEntity(x, y, z)
  {
    const transform = mat4.create();
    mat4.translate(transform, transform, [ x, y, z]);

    this.Entities.push(transform);
  }

  GetEntities() { return this.Entities; }
}

main();