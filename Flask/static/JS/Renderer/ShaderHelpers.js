//function CreateShaderProgram(gl, vsSource, fsSource)
//{
//  const vertexShader = LoadShader(gl, gl.VERTEX_SHADER, vsSource);
//  const fragmentShader = LoadShader(gl, gl.FRAGMENT_SHADER, fsSource);
//
//  let shaderProgram = gl.createProgram();
//  gl.attachShader(shaderProgram, vertexShader);
//  gl.attachShader(shaderProgram, fragmentShader);
//  gl.linkProgram(shaderProgram);
//
//  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS))
//  {
//    alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
//    shaderProgram = null;
//  }
//
//  return shaderProgram;
//}
//
//function LoadShader(gl, type, source)
//{
//  let shader = gl.createShader(type);
//  gl.shaderSource(shader, source);
//  gl.compileShader(shader);
//
//  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
//  {
//    alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
//    gl.deleteShader(shader);
//    shader = null;
//  }
//
//  return shader;
//}
//
//export { CreateShaderProgram };