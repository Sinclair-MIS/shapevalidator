#pragma once

#include <vector>
#include "Utils/Vector2.h"

using Point = Vector2;

class Points
{
	public:
		Points(std::vector<Vector2>&& points) :
			m_Points(std::move(points))
		{}

		template<typename... T_Args>
		void EmplaceVertex(T_Args&&... args) { m_Points.emplace_back(std::forward<T_Args>(args)...); }

		unsigned int GetCount() const noexcept { return m_Points.size(); }

		const Point& operator[](const unsigned int i) const noexcept { return m_Points[i]; }

		std::vector<Vector2>::iterator begin() noexcept { return m_Points.begin(); }
		std::vector<Vector2>::iterator end() noexcept { return m_Points.end(); }
		std::vector<Vector2>::const_iterator begin() const noexcept { return m_Points.begin(); }
		std::vector<Vector2>::const_iterator end() const noexcept { return m_Points.end(); }
		std::vector<Vector2>::const_iterator cbegin() const noexcept { return m_Points.cbegin(); }
		std::vector<Vector2>::const_iterator cend() const noexcept { return m_Points.cend(); }

	private:
		std::vector<Point> m_Points;
};