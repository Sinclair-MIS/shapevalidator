#pragma once

#include "Shapes/Line/Line.h"

class Polygon
{
	public:
		Polygon(const std::vector<Line>& parts) :
			m_Parts(parts)
		{}

		Polygon(std::vector<Line>&& parts) :
			m_Parts(std::move(parts))
		{}

		unsigned int LineCount() const { return static_cast<unsigned int>(m_Parts.size()); }
		const Line& operator[](const unsigned int i) const { return m_Parts[i]; }
		Line& operator[](const unsigned int i) { return m_Parts[i]; }

		std::vector<Line>::const_iterator begin() const { return m_Parts.begin(); }
		std::vector<Line>::const_iterator end() const { return m_Parts.end(); }

	private:
		std::vector<Line> m_Parts;

};
