#pragma once

#include <vector>

#include "../Line/Line.h"

class PolyLine
{
	public:	
		PolyLine(const std::vector<Line>& lines) :
			m_Lines(lines)
		{}

		PolyLine(std::vector<Line>&& lines) :
			m_Lines(std::move(lines))
		{}

		unsigned int LineCount() const { return static_cast<unsigned int>(m_Lines.size()); }
		const Line& operator[](const unsigned int i) const { return m_Lines[i]; }
		Line& operator[](const unsigned int i) { return m_Lines[i]; }

		std::vector<Line>::const_iterator begin() const { return m_Lines.begin(); }
		std::vector<Line>::const_iterator end() const { return m_Lines.end(); }

	private:
		std::vector<Line> m_Lines;
};