#pragma once

#include <vector>
#include "Utils/Vector2.h"

class Line
{
	public:
		Line(const std::vector<Vector2>& vertices) :
			m_Vertices(vertices)
		{}

		Line(std::vector<Vector2>&& vertices) :
			m_Vertices(std::move(vertices))
		{}

		unsigned int Length() const { return static_cast<unsigned int>(m_Vertices.size()); }

		Vector2& operator[](const unsigned int i) { return m_Vertices[i]; }
		const Vector2& operator[](const unsigned int i) const { return m_Vertices[i]; }

	private:
		std::vector<Vector2> m_Vertices;
};