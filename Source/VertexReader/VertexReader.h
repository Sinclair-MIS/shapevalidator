#pragma once

#include "VertexReader_Points/VertexReader_Points.h"
#include "VertexReader_Polygon/VertexReader_Polygon.h"
#include "VertexReader_PolyLine/VertexReader_PolyLine.h"

namespace VertexReader
{
	template<typename T>
	T Read(std::ifstream& in) { return T(); } // static_assert(false, "Unrecognised shape type");

	template<>
	Points Read(std::ifstream& in) { return VertexReader_Points::Read(in); }

	template<>
	Polygon Read(std::ifstream& in) { return VertexReader_Polygon::Read(in); }

	template<>
	PolyLine Read(std::ifstream& in) { return VertexReader_PolyLine::Read(in); }
}