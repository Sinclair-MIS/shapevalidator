#pragma once

#include <iostream>
#include "Shapes/Polygon/Polygon.h"

namespace VertexReader_Polygon
{
	Polygon Read(std::ifstream& in);
}