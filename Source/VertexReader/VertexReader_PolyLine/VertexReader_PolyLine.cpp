#include "VertexReader_PolyLine.h"

#include "Buffer/Buffer.h"
#include "Utils/Utils.h"

namespace VertexReader_PolyLine
{
	PolyLine Read(std::ifstream& in)
	{
		Buffer<16> readBuffer;

		in.seekg(std::streampos(24)); // Main File Header - Byte 24.
		readBuffer.Read(in, 16);
		unsigned int remainingRead = (static_cast<unsigned int>(Utils::ToInt_BE(readBuffer.Data())) * 2) - 100;

		std::streampos featureStart = 0;
		std::vector<Line> lines;
		while (remainingRead != 0)
		{
			in.seekg(featureStart + std::streampos(100)); // Record Header - Byte 0.
			readBuffer.Read(in, 12);
			const unsigned int recordNumber = static_cast<unsigned int>(Utils::ToInt_BE(readBuffer.Data()));
			const unsigned int contentLength = (static_cast<unsigned int>(Utils::ToInt_BE(readBuffer.Data() + 4)) * 2) + 8;

			in.seekg(featureStart + std::streampos(144)); // Record Contents - Byte 36.
			readBuffer.Read(in, 8);
			const unsigned int numParts = static_cast<unsigned int>(Utils::ToType<int>(readBuffer.Data()));
			const unsigned int numPoints = static_cast<unsigned int>(Utils::ToType<int>(readBuffer.Data() + 4));

			for (unsigned int i = 0; i < numParts; ++i)
			{
				in.seekg(featureStart + std::streampos(152 + (i * 4)));
				readBuffer.Read(in, 8);
				const unsigned int index_Start = static_cast<unsigned int>(Utils::ToType<int>(readBuffer.Data()));
				const unsigned int index_End = (i < numParts - 1) ? static_cast<unsigned int>(Utils::ToType<int>(readBuffer.Data() + 4)) : numPoints;
				const unsigned int numPoints_Part = index_End - index_Start;

				in.seekg(featureStart + std::streampos(152 + (numParts * 4) + (index_Start * 16)));

				std::vector<Vector2> line;
				line.reserve(numPoints_Part);
				for (unsigned int j = 0; j < numPoints_Part; ++j)
				{
					readBuffer.Read(in, 16);
					line.emplace_back(Utils::ToType<Vector2>(readBuffer.Data()));
				}
				lines.emplace_back(Line(std::move(line)));
			}

			remainingRead -= contentLength;
			featureStart += std::streampos(contentLength);
		}
		return PolyLine(lines);
	}
}