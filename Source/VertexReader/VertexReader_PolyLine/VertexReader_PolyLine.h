#pragma once

#include <iostream>
#include "Shapes/PolyLine/PolyLine.h"

namespace VertexReader_PolyLine
{
	PolyLine Read(std::ifstream& in);
}