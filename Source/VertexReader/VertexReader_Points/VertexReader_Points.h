#pragma once

#include <iostream>
#include "Shapes/Points/Points.h"

namespace VertexReader_Points
{
	Points Read(std::ifstream& in);
};