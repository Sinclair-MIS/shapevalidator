#include "VertexReader_Points.h"

#include "Buffer/Buffer.h"
#include "Utils/Utils.h"

namespace VertexReader_Points
{
	Points Read(std::ifstream& in)
	{
		Buffer<16> readBuffer;

		in.seekg(std::streampos(24)); // Main File Header - Byte 24.
		readBuffer.Read(in, 16);
		unsigned int remainingRead = (static_cast<unsigned int>(Utils::ToInt_BE(readBuffer.Data())) * 2) - 100;

		std::streampos featureStart = 0;
		std::vector<Vector2> points;

		in.seekg(featureStart + std::streampos(100)); // Record Header - Byte 0.
		readBuffer.Read(in, 8);
		const unsigned int recordNumber = static_cast<unsigned int>(Utils::ToInt_BE(readBuffer.Data()));
		const unsigned int contentLength = (static_cast<unsigned int>(Utils::ToInt_BE(readBuffer.Data() + 4)) * 2) + 8;
		while (remainingRead != 0)
		{
			in.seekg(featureStart + std::streampos(112)); // Record Contents - Byte 36.
			readBuffer.Read(in, 16);
			const Vector2 point = Utils::ToType<Vector2>(readBuffer.Data());
			points.emplace_back(point);
		
			remainingRead -= contentLength;
			featureStart += std::streampos(contentLength);
		}
		return Points(std::move(points));
	}
};