#include <iostream>
#include <string>
#include <thread>
#include <unordered_map>

#ifdef USE_FILESYSTEM_STANDARD
#include <filesystem>
namespace fs = std::filesystem;
#else
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#endif

#include "Utils/ShapeComponentTracker/ShapeComponentTracker.h"
#include "Utils/Vector2.h"
#include "Logger/Logger.h"
#include "Types.h"
#include "Utils/Utils.h"
#include "Validator/Validator.h"
#include "VertexReader/VertexReader.h"

std::string Test()
{
    return "ShapeValidator v0.0.1";
}

bool ValidateFile(const std::string& name);
bool Run(const std::string& name)
{
    bool shapeValid = false;

    Logger::DevMessage("Validating Set: %s", name.c_str());

    if (!fs::is_empty("TempStorage"))
    {
        std::unordered_map<std::string, ShapeComponentTracker> m_ComponentTrackers;
        for (const fs::directory_entry& entry : fs::directory_iterator("TempStorage\\" + name))
        {
            m_ComponentTrackers[entry.path().stem().string()].ComponentFound(entry.path().extension().string());
        }

        switch (m_ComponentTrackers[name].GetTrackerResult())
        {
            case TrackerResult::OK:
            {
                shapeValid = ValidateFile(name);
            }
            case TrackerResult::MISSING_COMPONENT: { Logger::Warning("Missing component: %s\nThis is possibly the result of two or more shape files having the same name.", name.c_str());  break; }
            case TrackerResult::DUPLICATE_COMPONENT: { Logger::Warning("Duplicate component: %s\nThis is possibly the result of two or more shape files having the same name.", name.c_str()); break; }
        }       
    }    

    return shapeValid;
}

template<typename T>
bool Validate(std::ifstream& file_In)
{
    T shape = VertexReader::Read<T>(file_In);
    return Validator::Validate(shape);
}

bool ValidateFile(const std::string& name)
{
    bool valid = false;

    Logger::Message("Validating file: %s", name.c_str());
    std::ifstream file_In("Resources/Shape_In/" + name + ".shp", std::ifstream::binary);

    if (file_In.is_open())
    {
        Buffer<8> readBuffer;

        file_In.seekg(32);
        readBuffer.Read(file_In, 4);

        const int shapeType = Utils::ToType<int>(readBuffer.Data());
        file_In.seekg(100);

        switch (shapeType)
        {
            case 1: { valid = Validate<Points>(file_In); break; }
            case 3: { valid = Validate<PolyLine>(file_In); break; }
            case 5: { valid = Validate<Polygon>(file_In); break; }
            default: { Logger::Error("Unrecognised shape type: %i", shapeType); }
        };
    }
    else
    {
        Logger::Error("Failed to open file for read: Resources/Shape_In/%s.shp", name.c_str());
    }

    return valid;
}

#ifdef PYTHON_BUILD
#include <Python.h>
#include <boost/python.hpp>

BOOST_PYTHON_MODULE(ShapeValidator)
{
    boost::python::def("ValidateFile", &ValidateFile);
    boost::python::def("Test", &Test);
}
#endif