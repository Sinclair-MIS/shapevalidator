// Name: Buffer
// Author: Sinclair Ross - sinclair@mckenzieintelligence.co.uk
// Notes: Contains an array of bytes which can be read from or written to. 
//        As memory is a limited resource for the ShapeShifter application this buffer allows us to control how much memory we use with greater precision.
//        Overflow behaviour - A function called when the a read will overflow the buffer. 

#pragma once

#include <array>
#include <algorithm> 
#include <fstream>
#include <functional>
#include <math.h>
#include <memory>
#include <string>
#include <type_traits>

#include "Logger/Logger.h"

template<unsigned int T_Length>
class Buffer
{
    private:
        Buffer(const std::function<void(const char*, const unsigned int)>& func_OverflowBehaviour) :
            m_Data(std::make_unique<std::array<char, T_Length>>()),
            m_End(0),
            m_Func_OverflowBehaviour(func_OverflowBehaviour)
        {}

    public:
        Buffer() :
            Buffer([](const char*, const unsigned int) { Logger::Error("Buffer overflow"); })
        {}

        Buffer(std::ofstream& writeFile) :
            Buffer(
                [&](const char* buff, const unsigned int size)
                {
                    writeFile.write(m_Data->data(), m_End);
                    m_End = 0;
                    Append(buff, size);
                })
        {}

            ~Buffer() = default;
            Buffer(const Buffer&) = delete;
            Buffer& operator=(const Buffer&) = delete;
            Buffer(Buffer&&) = delete;
            Buffer& operator=(Buffer&&) = delete;

            void Read(std::ifstream& file, const unsigned int size)
            {
                if (T_Length >= size)
                {
                    file.read(m_Data->data(), size);
                    m_End = size;
                }
                else
                {
                    file.read(m_Data->data(), T_Length);
                    m_End = T_Length;

                    const unsigned int size_Overflow = size - T_Length;
                    std::vector<char> tempBuff(size_Overflow);
                    file.read(tempBuff.data(), size_Overflow);
                    m_Func_OverflowBehaviour(tempBuff.data(), size_Overflow);
                }
            }

            void Append(const std::string& str) { Append(str.c_str(), str.size()); }
            void Append(const char* buff, const unsigned int size)
            {
                const unsigned int remainingSpace = RemainingSpace();
                if (remainingSpace > size)
                {
                    memcpy(m_Data->data() + m_End, buff, size);
                    m_End += size;
                }
                else
                {
                    memcpy(m_Data->data() + m_End, buff, remainingSpace);
                    m_End = T_Length;
                    m_Func_OverflowBehaviour(buff + remainingSpace, size - remainingSpace);
                }
            }

            void Clear() { m_End = 0; }
            void DeepClean() { m_End = 0; m_Data->fill(0); }
            void Delete(const unsigned int count) { m_End = (m_End > count) ? m_End - count : 0; }

            unsigned int GetCapacity() const { return T_Length; }
            unsigned int GetLength() const { return m_End; }
            const char* Data() const { return m_Data->data(); }
            unsigned int RemainingSpace() const { return T_Length - m_End; }

private:
    std::function<void(const char*, const unsigned int)> m_Func_OverflowBehaviour;
    std::unique_ptr<std::array<char, T_Length>> m_Data;
    unsigned int m_End;
};
