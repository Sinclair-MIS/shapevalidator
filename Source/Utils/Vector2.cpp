#include "Vector2.h"

#include <math.h>
#include "MathsHelper.h"

Vector2::Vector2(const double x, const double y) :
    X(x),
    Y(y)
{}

Vector2::Vector2() :
    Vector2(0.0f, 0.0f)
{}

bool Vector2::operator==(const Vector2& other) const { return fabs((X - other.X) < 0.0001f) && (fabs(Y - other.Y) < 0.0001f); }
bool Vector2::operator!=(const Vector2& other) const { return !(*this == other); }

Vector2 Vector2::operator-(const Vector2& rhs) const { return Vector2(X - rhs.X, Y - rhs.Y); }
Vector2 Vector2::operator+(const Vector2& rhs) const { return Vector2(X + rhs.X, Y + rhs.Y); }

Vector2 Vector2::operator*(const Vector2& rhs) const { return Vector2(X*rhs.X, Y*rhs.Y); }
Vector2 Vector2::operator*(const double rhs) const { return Vector2(X*rhs, Y*rhs); }

double Vector2::CalculateLength() const { return sqrtf(CalculateLength_Sqr()); }
double Vector2::CalculateLength_Sqr() const { return X*X + Y*Y; }

double Vector2::Dot(const Vector2& a, const Vector2& b) { return (a.X*b.X) + (a.Y*b.Y); }
double Vector2::Cross(const Vector2& a, const Vector2& b) { return (a.X * b.Y) - (b.X * a.Y); }

void Vector2::Normalise() 
{ 
    const double length = CalculateLength();
    if (length > 0.001f)
    {
        X /= length;
        Y /= length;
    }
}

Vector2 Vector2::Normalised() const
{
    Vector2 out(0,1);
    const double length = CalculateLength();
    if (length > 0.001f)
    {
        out.X = X / length;
        out.Y = Y / length;
    }
    return out;
}

std::pair<double, double> Vector2::IntersectsAt(const Vector2& a_Orig, const Vector2& a_Dest, const Vector2& b_Orig, const Vector2& b_Dest)
{
    const Vector2 a_Ray = a_Dest - a_Orig;
    const Vector2 b_Ray = b_Dest - b_Orig;

    const double a_Intercept = Vector2::Cross(b_Orig - a_Orig, b_Ray) / Vector2::Cross(a_Ray, b_Ray);
    const double b_Intercept = Vector2::Cross(a_Orig - b_Orig, a_Ray) / Vector2::Cross(b_Ray, a_Ray);

    return std::pair<double, double>(a_Intercept, b_Intercept);
}

bool Vector2::Intersects(const Vector2& a_Orig, const Vector2& a_Dest, const Vector2& b_Orig, const Vector2& b_Dest)
{
    std::pair<double, double> val = IntersectsAt(a_Orig, a_Dest, b_Orig, b_Dest);
    return MathsHelper::InRange(val.first, 0.0, 1.0) && MathsHelper::InRange(val.second, 0.0, 1.0);
}