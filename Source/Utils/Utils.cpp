#include "Utils.h"
#include "Logger/Logger.h"

namespace Utils
{
    int ToInt_BE(const char* buffer)
    {
        int out = -1;
        memcpy(&out, buffer, sizeof(int));

#ifdef WIN32 
        out = _byteswap_ulong(out);
#else
        out = __builtin_bswap32(out);
#endif

        return out;
    }

    std::string ToString(const int shapeType)
    {
        std::string str;
        switch (shapeType)
        {
        case 1: { str = "Point"; break; }
        case 3: { str = "MultiLineString"; break; } // LineString
        case 5: { str = "MultiPolygon"; break; } // Polygon
        case 8: { str = "MultiPoint"; break; }
        case 11: { str = "PointZ"; break; }
        case 13: { str = "PolyLineZ"; break; }
        case 15: { str = "PolygonZ"; break; }
        case 18: { str = "MultiPointZ"; break; }
        case 21: { str = "PointM"; break; }
        case 23: { str = "PolyLineM"; break; }
        case 25: { str = "PolygonM"; break; }
        case 28: { str = "MultiPointM"; break; }
        case 31: { str = "MultiPatch"; break; }
        default:
        {
            Logger::Warning("Unknown shape type: %i", shapeType);
            str = "<--- UNKNOWN SHAPE TYPE --->"; break;
        }
        }
        return str;
    }
}
