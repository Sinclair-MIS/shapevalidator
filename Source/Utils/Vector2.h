#pragma once

#include <tuple>

class Vector2
{
    public:
        Vector2();
        Vector2(const double x, const double y);
        bool operator==(const Vector2& other) const;
        bool operator!=(const Vector2& other) const;

        Vector2 operator-(const Vector2& rhs) const;
        Vector2 operator+(const Vector2& rhs) const;

        Vector2 operator*(const Vector2& rhs) const;
        Vector2 operator*(const double rhs) const;

        double CalculateLength() const;
        double CalculateLength_Sqr() const;

        void Normalise();
        Vector2 Normalised() const;

        static double Dot(const Vector2& a, const Vector2& b);
        static double Cross(const Vector2& a, const Vector2& b);
        static bool Intersects(const Vector2& a_Orig, const Vector2& a_Dest, const Vector2& b_Orig, const Vector2& b_Dest);
        static std::pair<double, double> IntersectsAt(const Vector2& a_Orig, const Vector2& a_Dest, const Vector2& b_Orig, const Vector2& b_Dest);

        double X, Y;
};

