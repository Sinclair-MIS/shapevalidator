// Name:   Utils
// Author: Sinclair Ross
// Notes:  A home for miscellaneous helper functions.

#pragma once

#include <cstring>
#include <string>

namespace Utils
{
    template <typename T>
    T ToType(const char* buffer)
    {
        T out;
        memcpy(&out, buffer, sizeof(T));
        return out;
    }

    int ToInt_BE(const char*);

    std::string ToString(const int shapeType);
}

