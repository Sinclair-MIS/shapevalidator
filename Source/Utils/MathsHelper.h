#pragma once

#include <cmath>

namespace MathsHelper
{
	enum class Angle
	{
		Deg,
		Rad
	};

	constexpr float PI = 3.141592653f;
	constexpr float FloatEpsilon = 0.0001f;

	template<typename T>
	constexpr T Clamp(T val, T min, T max)
	{
		return (max * (val > max)) +
			(min * (val < min)) +
			(val * (val >= min && val <= max));
	}

	template<typename T>
	constexpr T Normalise(T val, T min, T max)
	{
		return Clamp<T>((val - min) / (max - min), 0.0f, 1.0f);
	}

	template<typename T>
	constexpr T SignedNormalise(T val, T min, T max)
	{
		return Clamp((Normalise(val, min, max) - 0.5f) * 2, -1.0f, 1.0f);
	}

	template<typename T>
	constexpr T Wrap(T val, T min, T max)
	{
		T range = max - min + 1;

		if (val < min)
		{
			val += range * ((min - val) / range + 1.0f);
		}

		float output = min + fmod(val - min, range);
		return output;
	}

	constexpr int NextP2(int a)
	{
		int rval = 1;
		while (rval < a) { rval <<= 1; }
		return rval;
	}

	template<typename T>
	constexpr T Pow(T val, const unsigned int n)
	{
		for (unsigned int i = 0; i < n; ++i) { val *= val; }
		return val;
	}

	template<typename T>
	bool InRange(const T& a, const T& min, const T& max) { return a >= min && a <= max; }

	template<typename T>
	constexpr T Min(T a, T b) { return (a * (a < b)) + (b * (b <= a)); }

	template<typename T>
	constexpr T Max(T a, T b) { return (a * (a > b)) + (b * (b >= a)); }

	template<typename T>
	int Sign(T a) { return (a > static_cast<T>(0)) - (a < static_cast<T>(0)); }

	float acos(float val);

	constexpr float Lerp(float val, float min, float max) { return (min * (1.0f - val)) + (max * val); }
	constexpr float ToRadians(float degrees) { return degrees * (PI / 180); }
	constexpr float ToDegrees(float radians) { return radians * (180 / PI); }
}