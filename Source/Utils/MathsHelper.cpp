#include "MathsHelper.h"

namespace MathsHelper
{
	extern float acos(float val) { return std::acos(Clamp(val, -0.99998f, 0.99998f)); }
}