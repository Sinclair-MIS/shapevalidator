#include "ShapeComponentTracker.h"

#include "Logger/Logger.h"

ShapeComponentTracker::ShapeComponentTracker() :
    m_Flags(0)
{}

void ShapeComponentTracker::ComponentFound(const std::string& fileExtension)
{
    const ShapeComponentType component = StringToShapeComponentType(fileExtension);
    if (component != ShapeComponentType::_INVALID)
    {
        if (static_cast<bool>(m_Flags & static_cast<uint8_t>(component)))
        {
            m_Flags |= 32;
        }

        m_Flags |= static_cast<uint8_t>(component);
    }
}

TrackerResult ShapeComponentTracker::GetTrackerResult() const
{
    TrackerResult result = TrackerResult::_INVALID;
    if (m_Flags == 31) { result = TrackerResult::OK; }
    else if (m_Flags < 31) { result = TrackerResult::MISSING_COMPONENT; }
    else if (m_Flags > 32) { result = TrackerResult::DUPLICATE_COMPONENT; }

    return result;
}

ShapeComponentType ShapeComponentTracker::StringToShapeComponentType(const std::string& string) const
{
    ShapeComponentType type = ShapeComponentType::_INVALID;
    if (string == ".cpg" || string == ".cst") { type = ShapeComponentType::CPG; }
    else if (string == ".dbf") { type = ShapeComponentType::DBF; }
    else if (string == ".prj") { type = ShapeComponentType::PRJ; }
    else if (string == ".shp") { type = ShapeComponentType::SHP; }
    else if (string == ".shx") { type = ShapeComponentType::SHX; }
    else { Logger::Warning("Unknown file extension: %s", string.c_str()); }

    return type;
}