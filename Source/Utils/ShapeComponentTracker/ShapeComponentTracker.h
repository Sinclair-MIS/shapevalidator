// Name: ShapeComponentTracker
// Author: Sinclair Ross
// Notes: Used to assure that there are 5 and only 5 components in a shape file.
//      m_HasComponents -> A byte containing, flags 1-5 representing the presense of one of the 5 shape file extensions.
//                         flag 6 representing the duplicate flag, this will be set when the component tracker reads a duplicate.
//                         flags 7 & 8 are unused.

#pragma once

#include <cstdint>
#include <string>

enum class TrackerResult
{
    OK,
    MISSING_COMPONENT,
    DUPLICATE_COMPONENT,

    _COUNT,
    _INVALID
};

enum class ShapeComponentType : uint8_t
{
    CPG = 1,
    DBF = 2,
    PRJ = 4,
    SHP = 8,
    SHX = 16,

    _INVALID
};

class ShapeComponentTracker
{
    public:
        ShapeComponentTracker();

        void ComponentFound(const std::string& fileExtension);
        TrackerResult GetTrackerResult() const;

    private:
		ShapeComponentType StringToShapeComponentType(const std::string& string) const;

		uint8_t m_Flags;
};
