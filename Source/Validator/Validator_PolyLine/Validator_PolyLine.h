#pragma once

#include <vector>

#include "Shapes/PolyLine/PolyLine.h"

namespace Validator_PolyLine
{
    bool Validate(const PolyLine& lines);
}
