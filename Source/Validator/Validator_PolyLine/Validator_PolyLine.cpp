#include "Validator_PolyLine.h"

#include <functional>

#include "Utils/Vector2.h"
#include "Logger/Logger.h"

namespace Validator_PolyLine
{
    bool Validate(const PolyLine& lines)
    {
        bool valid = true;

        for (const Line& line : lines)
        {
            if (const bool ring = (line[0] == line[line.Length()-1]); ring)
            {
                if (line.Length() < 4)
                {
                    valid = false;
                    Logger::Warning("ring %i with less than four points", 0);
                }

                if (line[0] != line[line.Length() - 1])
                {
                    valid = false;
                    Logger::Warning("ring %i not closed", 0);
                }
            }
            else if (line.Length() < 2)
            {
                valid = false;
                Logger::Warning("line %i with less than two points", 0);
            }

            for (unsigned int i = 0; i < line.Length() - 1; ++i)
            {
                if (line[i] == line[i + 1])
                {
                    valid = false;
                    Logger::Warning("line %i contains duplicate nodes", 0);
                    break;
                }
            }

            unsigned int numBowties = 0;
            for (unsigned int i = 1; i < line.Length()-1; ++i)
            {
                for (unsigned int j = 0; j < (i-1); ++j)
                {
                    if (Vector2::Intersects(line[i], line[i + 1], line[j], line[j + 1]))
                    {
                        ++numBowties;
                    }
                }
            }

            if (numBowties > 0)
            {
                Logger::Warning("%i bowties detected", numBowties);
                valid = false;
            }
        }

        return valid;
    }
}