#pragma once

#include <functional>
#include <vector>

#include "Utils/Vector2.h"
#include "Validator_Points/Validator_Points.h"
#include "Validator_Polygon/Validator_Polygon.h"
#include "Validator_PolyLine/Validator_PolyLine.h"
#include "Shapes/Points/Points.h"
#include "Shapes/Polygon/Polygon.h"
#include "Shapes/PolyLine/PolyLine.h"

namespace Validator
{
	template<typename T>
	bool Validate(const T& shape) { return false; } // static_assert(false, "Unrecognised shape type");

	template<>
	bool Validate(const Points& shape) { return Validator_Points::Validate(shape); }

	template<>
	bool Validate(const Polygon& shape) { return Validator_Polygon::Validate(shape); }

	template<>
	bool Validate(const PolyLine& shape) { return Validator_PolyLine::Validate(shape); }
}