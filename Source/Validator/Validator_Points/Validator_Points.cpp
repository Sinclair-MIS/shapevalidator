#include "Validator_Points.h"

#include "Logger/Logger.h"

namespace Validator_Points
{
	bool Validate(const Points& points)
	{
		bool valid = true;

		for (unsigned int i = 0; i < points.GetCount(); ++i)
		{
			const Point& point = points[i];

			valid = abs(point.X) <= 90.0f && abs(point.Y) <= 180.0f;
			if (!valid) 
			{ 
				Logger::Warning("Point %i - Outside of range (-90 <= X <= 90, -180 <= Y <= 180)", i);
				break; 
			}
		}

		return valid;
	}
}