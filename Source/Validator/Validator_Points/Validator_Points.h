#pragma once

#include <vector>

#include "Shapes/Points/Points.h"

namespace Validator_Points
{
    bool Validate(const Points& points);
}
