#include "Validator_Polygon.h"

#include <functional>

#include "Utils/MathsHelper.h"
#include "Logger/Logger.h"

namespace Validator_Polygon
{
    bool Validate(const Polygon& polygon)
    {     
        std::function<bool(const Line&)> test_Length = [](const Line& line) -> bool { return line.Length() > 3; };
        std::function<bool(const Line&)> test_Loop = [](const Line& line) -> bool { return line[0] == line[line.Length() - 1]; };
        std::function<bool(const Line&)> test_SelfIntersection = [](const Line& line) -> bool
        {
            bool test = true;
            for (unsigned int i = 1; i < line.Length(); ++i)
            {
                for (unsigned int j = 1; j < i - 1; ++j)
                {
                    const std::pair<double, double> intersectsAt = Vector2::IntersectsAt(line[i - 1], line[i], line[j - 1], line[j]);
                    test = !MathsHelper::InRange(intersectsAt.first, 0.000001, 0.999999) && !MathsHelper::InRange(intersectsAt.second, 0.000001, 1.0);
                    if (!test) { break; }
                }
                if (!test) { break; }
            };

            return test;
        };

        bool valid = true;

        for (const Line& line : polygon)
        {
            const bool length = test_Length(line);
            const bool loop = test_Loop(line);
            const bool selfIntersection = test_SelfIntersection(line);

            valid = length && loop && selfIntersection;
            if (!valid) { break; }    
        }

        return valid;
    }
}
