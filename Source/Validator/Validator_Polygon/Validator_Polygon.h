#pragma once

#include <vector>

#include "Shapes/Polygon/Polygon.h"

namespace Validator_Polygon
{
    bool Validate(const Polygon& polygon);
}
