#include "Logger.h"

#include <fstream>
#include <iomanip>
#include <memory>

#ifdef _WIN32
#include <Windows.h>
#endif

#ifdef USE_FILESYSTEM_STANDARD
    #include <filesystem>
    namespace fs = std::filesystem;
#else
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#endif

namespace Logger
{
    namespace Internal
    {
        #ifdef _WIN32
        void SetConsoleColour(const ConsoleColour colour)
        {
            HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

            switch (colour)
            {
                case ConsoleColour::WHITE: { SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE); break; }
                case ConsoleColour::RED: { SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY | FOREGROUND_RED); break; }
                case ConsoleColour::YELLOW: { SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_RED); break; }
                case ConsoleColour::CYAN: { SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE); break; }
            }
        }
        #else
        void SetConsoleColour(const ConsoleColour colour) {}
        #endif

        void OutputToConsole(const char* strBuffer, ConsoleColour colour)
        {
            SetConsoleColour(colour);
            printf(strBuffer);
            SetConsoleColour(ConsoleColour::WHITE);
        }

        void OutputToFile(const char* strBuffer, const char* outputFile)
        {
            if (!fs::exists("Logs")) 
            { 
                fs::create_directory("Logs"); 
            }

            std::ofstream file_Out(outputFile, std::ofstream::app);
            if(file_Out)
            {
                file_Out.write(strBuffer, strlen(strBuffer));
                file_Out.close();
            }
            else
            {
                const std::string errorMsg("Could not open file: " + std::string(outputFile) + "");
                OutputToConsole(errorMsg.c_str(), ConsoleColour::RED);
            }
        }

        void DatedLog(const char* strBuffer)
        {
            time_t rawtime;
            time(&rawtime);
            struct tm* timeinfo = localtime(&rawtime);

            char buffer[18];
            strftime(buffer, 18, "%d-%m-%Y", timeinfo);
            puts(buffer);

            std::stringstream ss;
            ss << "Logs/" << buffer << ".txt";
            OutputToFile(strBuffer, ss.str().c_str());
        }
    }
}
